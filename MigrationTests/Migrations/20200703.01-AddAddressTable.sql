create table Addresses (
    Id uniqueidentifier not null,
    Address1 nvarchar(128),
    Address2 nvarchar(128),
    City nvarchar(128),
    State nvarchar(2),
    ZipCode nvarchar(10)
);

GO

alter table Addresses
    add constraint PK_Addresses_Id primary key clustered (Id);

go