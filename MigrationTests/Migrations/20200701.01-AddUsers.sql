if not exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'Users')
begin
    CREATE TABLE Users (
        Id uniqueidentifier not null,
        FirstName nvarchar(64) not null,
        LastName nvarchar(64) not null,
    );
end
GO;

if not exists(select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_NAME = 'PK_Users_Id')
begin
    alter table Users
        add constraint PK_Users_Id primary key clustered (Id);
end;
GO;