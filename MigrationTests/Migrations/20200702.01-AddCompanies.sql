create table Companies (
    Id uniqueidentifier not null,
    Name nvarchar(128),
    AddressId uniqueidentifier not null
);

go

alter table Companies
    add constraint PK_Companies_Id primary key clustered (Id);

go