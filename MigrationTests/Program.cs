﻿using System;
using System.Reflection;
using DbUp;

namespace MigrationTests
{
    class Program
    {
        private static string ConnectionString = "Data Source=localhost;Initial Catalog=MigrationTesting;User ID=SA;Password=Password1!;Pooling=False;"; 
        
        static void Main(string[] args)
        {
            var upgrader = DeployChanges.To
                .SqlDatabase(ConnectionString)
                .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                .LogToConsole()
                .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                Console.WriteLine("\n\nshit done broke!\n\n");
            }

        }
    }
}